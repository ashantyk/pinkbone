<?php if(!defined("BASEPATH")) die("Restricted access!");

$config = array();

// database connection settings
$config['database'] = array(
    'driver'   => "MySQLi",
    'hostname' => "{hostname}",
    'username' => "{username}",
    'password' => "{password}",
    'database' => "{database}",
    'port'     => {port},
);

$config['logger'] = array(
    "level" => 2, // lower is more verbose [0-4]
    "path" => "Logs/%Y-%m-%d.log" // 2016-11-20.log (see strftime for format)
);

$config['cache'] = array(
    "enabled" => false,
    "driver" => "FileCache",
    "dir" => sys_get_temp_dir()
);

$config['router'] = array(
    "reader"         => null,
    "writer"         => null
);

$config['app'] = array(
    "folder"            => "App",
    "defaultController" => "Main",
    "defaultMethod"     => "index",
    "errorPage"         => null,
    "overrides"         => null,
    "securityManager"   => null,
    "settingsStore"     => null
);
