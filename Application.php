<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Exceptions\RoutingException;
use \System\Exceptions\SecurityException;
use \System\Exceptions\ApiException;
use \System\Logger;

final class Application {

    /**
     * @var \System\Application Holds the app instance
     */
    private static $_instance = null;

    /**
     * @var \System\Base\SecurityManager Holds the app security manager instance
     */
    private $_securityManager = null;

    /**
     * @var \System\CacheManager Holds the app cache manager instance
     */
    private $_cacheManager = null;

    /**
     * @var \System\ConfigManager Holds the app config manager instance
     */
    private $_configManager = null;

    /**
     * @var \System\Router Holds the app router instance
     */
    private $_router = null;

    /**
     * @var \System\Database
     */
    private $_db = null;

    /**
     * @var array List of controller instances
     */
    private $_controllers = array();

    /**
     * @var array List of store instances
     */
    private $_stores = array();

    /**
     * Gets the application instance (with config loaded)
     *
     * @return \System\Application
     */
    public static function getInstance(){

        // check instance
        if(!self::$_instance){
            
            // create instance
            self::$_instance = new static();

            // load config
            self::$_instance->_configManager = new \System\ConfigManager(self::$_instance);

        }

        // return instance
        return self::$_instance;

    }

    // overridden functions for singleton pattern
    protected function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    /**
     * Gets configuration settings by group
     *
     * @param string $group
     * @param string $key
     * @return mixed
     */
    public function getConfig($group = null, $key = null){
        return $this->_configManager->get($group, $key);
    }

    /**
     * Updates configuration settings by group and key
     *
     * @param string $group
     * @param string $key
     * @param string $value
     * @param bool|null $autoload
     * @return bool
     */
    public function setConfig($group, $key, $value, $autoload = null){
        return $this->_configManager->set($group, $key, $value, $autoload);        
    }

    /**
     * Gets the Database Driver instance
     *
     * @return \System\Database
     */
    public function &getDb(){

        if(!$this->_db){

            $databaseConfig = $this->getConfig("database");

            $this->_db = new \System\Database($databaseConfig);

        }

        return $this->_db;

    }

    /**
     * Gets the Cache Driver instance
     *
     * @return \System\CacheManager
     */
    public function &getCacheManager(){

        if(!$this->_cacheManager){

            $cacheConfig = $this->getConfig("cache");

            $this->_cacheManager = new \System\CacheManager($cacheConfig);

        }

        return $this->_cacheManager;

    }

    /**
     * Gets the Security Manager instance
     *
     * @return \System\Base\AbstractSecurityManager
     */
    public function &getSecurityManager(){
        return $this->_securityManager;
    }

    /**
     * Gets the Router instance
     *
     * @return \System\Router
     */
    public function &getRouter(){

        if(!$this->_router){
            // create security manager instance
            $this->_router = new \System\Router($this);
        }

        return $this->_router;

    }

    /**
     * Gets a certain controller
     *
     * @param string $controllerName
     * @return mixed
     */
    public function &getController( $controllerName ){

        if(!isset($this->_controllers[$controllerName])){
            $fullControllerName = $this->getConfig("app", "folder") . "\\Controller\\" . $controllerName;
            $this->_controllers[$controllerName] = new $fullControllerName( $this );
        }

        return $this->_controllers[$controllerName];

    }

    /**
     * Gets a certain store
     *
     * @param string $storeName
     * @return mixed
     */
    public function &getStore($storeName){

        if(!isset($this->_stores[$storeName])){
            $fullStoreName = $this->getConfig("app", "folder") . "\\Store\\" . $storeName;
            $this->_stores[$storeName] = new $fullStoreName( $this );
        }

        return $this->_stores[$storeName];

    }

    /**
     * Sets a certain store
     * Usefull in unit tests
     *
     * @param string $storeName
     * @param object $storeInstance
     */
    public function setStore($storeName, $storeInstance){

        $this->_stores[$storeName] = $storeInstance;

    }

    /**
     * Checks if application is running in service mode
     *
     * @return bool
     */
    public function isServiceMode(){
        $sapiName = php_sapi_name();
        return ($sapiName == 'cli' || $sapiName == 'cgi-fcgi');
    }

    /**
     * Stuff to do before app starts handling the request
     *
     */
    protected function _preRun(){

        // load db settings
        $this->_configManager->initDatabaseSettings();

        // check overrides to load
        $overridesFile = $this->getConfig("app", "overrides");
        if($overridesFile){
            require_once(BASEPATH . DIRECTORY_SEPARATOR . $overridesFile);
        }

        // check security manager
        $securityManagerClass = $this->getConfig("app", "securityManager");
        if($securityManagerClass){
            $this->_securityManager = new $securityManagerClass( $this );
        }

    }

    /**
     * Stuff to do after the app finished handling the request
     *
     */
    protected function _postRun(){

    }

    /**
     * Handles the request
     *
     * @param string|null $class Override the class detected by the router
     * @param string|null $method Override the method detected by the router
     * @param array|null $params Override the params detected by the router
     */
    public function run($class = null, $method = null, $params = null){

        $this->_preRun();

        $output = "";

        try {

            // create router
            $router = $this->getRouter();

            // get class name
            $className = is_null($class) ? $router->getClassName() : $class;
            
            // get class with namespace
            $class = $this->getConfig("app", "folder") . "\\Controller\\" . $className;

            // check class existance
            $classFilePath = BASEPATH . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
            if(!is_file($classFilePath)){
                throw new RoutingException("Invalid class: " . $className, RoutingException::CLASS_NOT_FOUND );
            }

            // instantiate class (controller)
            $controller = $this->getController($className);

            // get method
            $methodName = is_null($method) ? $router->getMethod() : $method;

            // validate method
            if(substr($methodName, 0, 1) === '_' || !method_exists($controller, $methodName)){
                throw new RoutingException("Invalid method: " . $methodName, RoutingException::METHOD_NOT_FOUND );
            }

            // get params
            $params = !is_array($params) ? $router->getParams($className) : $params;

            // get required rights to run this call
            $requiredRights = $router->getRequiredRights();

            // check if call requires certain access rights
            if(!in_array("none", $requiredRights)){

                $securityManager = $this->getSecurityManager();

                // redirect to login
                if(!$this->isServiceMode() && !$securityManager->isAuthenticated()){
                    $this->redirect($securityManager->getLoginPageURL());
                }

                // check rights
                if(!$this->getSecurityManager()->currentUserHasRights($requiredRights)){
                    throw new SecurityException("You are not allowed to access this resource!", SecurityException::NOT_ENOUGH_RIGHTS);
                }

            }

            if($cacheDuration = $router->hasCacheFlag()){ // this page is included in page caching

                // get cache manager
                $cacheManager = $this->getCacheManager();
                
                // determine cache ckey to check
                $cacheKey = $cacheManager->buildCacheKey($className, $methodName, $params);

                // check cache
                $output = $cacheManager->get($cacheKey);

                if($output === null){ // no cache

                    // exec method
                    $output = call_user_func_array(array($controller, $methodName), $params);

                    if($output !== null){ // cache if call successfull and with data
                        if(is_int($cacheDuration)){
                            $cacheManager->set($cacheKey, $output, $cacheDuration);
                        } else {
                            $cacheManager->set($cacheKey, $output);
                        }
                    }

                }

            } else { // skip cache check

                // exec method
                $output = call_user_func_array(array($controller, $methodName), $params);

            }

            // print output
            $router->output($output);


        } catch (\Exception $ex){

            $router->outputError($ex);  

        }

        $this->_postRun();
        
    }

    /**
     * Trigger a redirect to a specific url
     *
     * @param string $url
     */
    public function redirect( $url ){
        header("Location: " . $url);
        exit();
    }

    /**
     * Sets a flash data that will be loaded on next user request
     *
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    public function setFlashData($name, $value){

        if (session_status() == PHP_SESSION_NONE) {
            return false;
        }

        if(!isset($_SESSION['_flashData'])){
            $_SESSION['_flashData'] = array();
        }

        $_SESSION['_flashData'][$name] = $value;

        return true;

    }

    /**
     * Gets the flash data from user's previous request
     *
     * @param bool $clear
     * @return array|null
     */
    public function getFlashData( $clear = true ){

        if (session_status() == PHP_SESSION_NONE) {
            return null;
        }

        $data = isset($_SESSION['_flashData']) ? $_SESSION['_flashData'] : null;

        if($clear && isset($_SESSION['_flashData'])){
            unset($_SESSION['_flashData']);
        }

        return $data;

    }

}
