<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Exceptions\RoutingException;

class Router {

    private $_app;

    private $_reader;
    private $_writer;

    private $_cache = false;
    private $_templateName = null;
    private $_requiredRights = array("none");
    private $_headers = array();

    /**
     * Router constructor
     * 
     * @param \System\Application $app
     */
    public function __construct( &$app ){
        $this->_app =& $app;
    }

    /**
     * Get writer
     *
     * @return \System\Writers\AbstractWriter
     * @throws \Exception
     */
    public function &getWriter(){

        if(!$this->_writer){

            $configWriter = $this->getApp()->getConfig('router', 'writer');

            if(!$configWriter){
                throw new \Exception("Router 'writer' is not configured!");
            }

            $this->_writer = new $configWriter($this);

        }

        return $this->_writer;
    
    }

    /**
     * Get reader
     *
     * @return \System\Readers\AbstractReader
     * @throws \Exception
     */
    public function &getReader(){

        if(!$this->_reader){

            $configReader = $this->getApp()->getConfig('router', 'reader');

            if(!$configReader){
                throw new \Exception("Router 'reader' is not configured!");
            }

            $this->_reader = new $configReader();

        }

        return $this->_reader;
    
    }

    /**
     * Get application instance
     * @return \System\Application
     */
    public function &getApp(){
        return $this->_app;
    }

    public function getClassName(){

        $className = $this->getReader()->getClassName();
        
        if(!$className){
            $className = $this->getApp()->getConfig("app", "defaultController");
        }

        return $className;

    }

    public function getMethod(){

        $method = $this->getReader()->getMethod();
        
        if(!$method){
            $method = $this->getApp()->getConfig("app", "defaultMethod");
        }

        return $method;

    }

    protected function _parseMeta($meta){

        if(!$meta){
            return;
        }

        if(preg_match("/@cache ([0-9]+)/", $meta, $matches)){
            $this->_cache = intval($matches[1]);
        } elseif(strpos($meta, "@cache") !== false){
            $this->_cache = true;
        }

        if(preg_match("/@view ([\w\/]+)/", $meta, $matches)){
            $this->_templateName = $matches[1];
        }

        if(preg_match("/@requireRights ([a-zA-Z ]+)/", $meta, $matches)){
            $rawRights = trim(preg_replace('!\s+!', ' ', $matches[1]));
            $this->_requiredRights = explode(" ", $rawRights);
        }

        if(preg_match("/@writer ([\w\\\]+)/", $meta, $matches)){
            $writerClass = $matches[1];
            $this->_writer = new $writerClass($this);
        }

    }

    public function getParams($controllerName){

        $params = array();
        $paramsFromReader = $this->getReader()->getParams();

        $method = $this->getMethod();
        $methodReflection = new \ReflectionMethod($this->getApp()->getController($controllerName), $method);

        $meta = $methodReflection->getDocComment();
        $this->_parseMeta($meta);

        $methodParams = $methodReflection->getParameters();

        foreach ($methodParams as $param) {
        
            $paramName = $param->getName();
    
            if(array_key_exists($paramName, $paramsFromReader)){
                $params[] = $paramsFromReader[$paramName];
            } else if($param->isOptional()){
                break;
            } else {
                throw new RoutingException(_("Missing parameter") . ": " . $paramName . " (" . $this->getReader()->getClassName() . "::" . $method . ")", RoutingException::MISSING_PARAMETER);
            }

        }

        return $params;

    }

    public function getTemplateName(){
        return $this->_templateName;
    }

    public function setTemplateName( $templateName ){
        $this->_templateName = $templateName;
    }

    public function hasCacheFlag(){
        return $this->_cache;
    }

    public function output($data){

        // get writer
        $writer = $this->getWriter();

        // set headers
        $headers = array_merge($this->_headers, $writer->getHeaders());

        // set headers
        foreach ($headers as $key => $value) {
            if(is_null($value)){
                header($key);
            } else {
                header($key . ": " . $value);
            }
        }

        // prevent successfull requests from filling up cron log
        if($this->getApp()->isServiceMode() && (!is_array($data) && !is_string($data))){
            return;
        }

        $writer->output($data, $this->getTemplateName());

    }

    public function outputError($error){
        $this->getWriter()->output($error, $this->getApp()->getConfig("app", "errorPage"));
    }

    public function getRequiredRights(){
        return $this->_requiredRights;
    }

    public function setRawHeader($header){
        $this->_headers[$header] = null;
    }

    public function setHeader($headerName, $headerValue){
        $this->_headers[$headerName] = $headerValue;
    }

    public function getHeader($headerName){
        return $this->_headers[$headerName];
    }

    public function unsetHeader($headerName){
        unset($this->_headers[$headerName]);
    }

}