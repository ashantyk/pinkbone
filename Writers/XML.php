<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Writers\AbstractWriter;

class XML extends AbstractWriter {

    public function output( $data, $template = null ){

        if(is_null($template)){
            http_response_code(400);
            return;
        }

        switch (true) {
            case $data instanceof \System\Exceptions\RoutingException:
                http_response_code(404);
                return;
            case $data instanceof \System\Exceptions\SecurityException:
                http_response_code(401);
                return;
            case $data instanceof \Exception:
                \System\Logger::error(get_class($data), $data->getMessage());
                http_response_code(500);
                return;
        }

        define("THEME_DIR", implode( DIRECTORY_SEPARATOR, array(
            BASEPATH,
            $this->getRouter()->getApp()->getConfig("app", "folder"),
            "View"            
        )));

        $viewPath = THEME_DIR . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $template) . ".php";
       
        ob_start();

        if(isset($data) && $data){
            extract($data);
        }

        require($viewPath);
        
        $output = ob_get_contents();

        ob_end_clean();

        echo $output;
        
    }

    public function getHeaders(){

        return array(
            'Content-Type' => "text/xml"
        );

    }

}
