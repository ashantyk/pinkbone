<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Writers\AbstractWriter;

class HTML extends AbstractWriter {

    public function output( $data, $template = null ){

        switch (true) {

            case $data instanceof \System\Exceptions\RoutingException:
                http_response_code(404);
                $data =  array(
                    "title" => _("404 - Page not found"),
                    "message" => _("We could not find the requested page on our servers.")
                );
                break;

            case $data instanceof \System\Exceptions\SecurityException:
                http_response_code(401);
                $data =  array(
                    "title" => _("401 - Unauthorized"),
                    "message" => _("You are not allowed to access this resources.")
                );
                break;

            case $data instanceof \Exception:
                \System\Logger::error(get_class($data), $data->getMessage());
                http_response_code(500);
                $data =  array(
                    "title" => _("500 - Internal Server Error"),
                    "message" => _("A server error occured. An issue has been logged and will be looked into it.")
                );
                break;
                
        }

        if(is_null($template)){
            return;
        }

        $appInstance = $this->getRouter()->getApp();
        
        $flashData = $appInstance->getFlashData();
        if(is_array($flashData) && is_array($data)){
            $data = array_merge($flashData, $data);
        }
        
        define("THEME_DIR", implode( DIRECTORY_SEPARATOR, array(
            BASEPATH,
            $appInstance->getConfig("app", "folder"),
            "View"            
        )));

        $viewPath = THEME_DIR . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $template) . ".php";
       
        ob_start();

        if(!empty($data) && is_array($data)){
            extract($data);
        }

        require($viewPath);
        
        $output = ob_get_contents();

        ob_end_clean();

        echo $output;

    }

    public function getHeaders(){

        return array(
            'Content-Type' => "text/html",
            'Strict-Transport-Security' => "max-age=31536000; includeSubDomains",
            'X-Frame-Options' => "SAMEORIGIN",
            'X-Content-Type-Options' => 'nosniff',
            'X-XSS-Protection' => "1; mode=block"
            //'Content-Security-Policy' => "default-src 'self' ajax.googleapis.com www.google-analytics.com fonts.googleapis.com www.gstatic.com fonts.gstatic.com www.google.com; unsafe-inline 'self'; img-src 'self';"
        );

    }

}
