<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class AbstractWriter {

    public function __construct(&$router){
        $this->_router =& $router;
    }

    /**
     *
     * @param mixed $data
     * @param string|null $template
     */
    abstract public function output( $data, $template = null );

    /**
     * @return array
     */
    public function getHeaders(){
        return array();
    }

    /**
     * @return \System\Router
     */
    public function &getRouter(){
        return $this->_router;
    }

}
