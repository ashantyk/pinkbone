<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Writers\AbstractWriter;

class JsonRPC extends AbstractWriter {

    public function output( $data, $template = null ){

        $response = array();

        switch (true) {
            case $data instanceof \System\Exceptions\RoutingException:
                switch ($data->getCode()) {
                    case \System\Exceptions\RoutingException::CLASS_NOT_FOUND:
                    case \System\Exceptions\RoutingException::METHOD_NOT_FOUND:
                        $response['error'] = array(
                            "code"    => \System\Exceptions\JsonRpcException::METHOD_NOT_FOUND,
                            "message" => _("Method not found.")
                        );
                        break;
                    case \System\Exceptions\RoutingException::MISSING_PARAMETER:
                    case \System\Exceptions\RoutingException::RESOURCE_NOT_FOUND:
                        $response['error'] = array(
                            "code"    => \System\Exceptions\JsonRpcException::INVALID_PARAMS,
                            "message" => _("Invalid params.")
                        );
                        break;
                    default:
                        $response['error'] = array(
                            "code"    => \System\Exceptions\JsonRpcException::INTERNAL_ERROR,
                            "message" => _("Internal error.")
                        );
                        break;
                }
                break;

            case $data instanceof \System\Exceptions\SecurityException:
                $response['error'] = array(
                    "code"    => \System\Exceptions\JsonRpcException::UNAUTHORIZED,
                    "message" => _("Unauthorized.")
                );
                break;

            case $data instanceof \System\Exceptions\JsonRpcException:
                $response['error'] = array(
                    "code"    => $data->getCode(),
                    "message" => $data->getMessage()
                );
                break;

            case $data instanceof \Exception:
                \System\Logger::error(get_class($data), $data->getMessage());
                $response['error'] = array(
                    "code"    => \System\Exceptions\JsonRpcException::INTERNAL_ERROR,
                    "message" => _("Internal error.")
                );
                break;

            default:
                $response['result'] = $data;
                
        }

        if(isset($response['error'])){
            http_response_code($this->getStatusCodeForErrorCode($response['error']['code']));
        }

        $response['jsonrpc'] = $this->getRouter()->getReader()->getVersion();
        $response['id'] = $this->getRouter()->getReader()->getID();

        echo json_encode($response);
      
    }

    protected function getStatusCodeForErrorCode( $errorCode ){

        if($errorCode === \System\Exceptions\JsonRpcException::INVALID_REQUEST){
            return 400;
        } elseif($errorCode === \System\Exceptions\JsonRpcException::UNAUTHORIZED){
            return 401;
        } elseif($errorCode === \System\Exceptions\JsonRpcException::METHOD_NOT_FOUND){
            return 404;
        }

        return 500;

    }

    public function getHeaders(){

        return array(
            'Content-Type' => "application/json"
        );

    }

}
