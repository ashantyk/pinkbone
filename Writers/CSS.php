<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Writers\AbstractWriter;

class CSS extends AbstractWriter {

    public function output( $data, $template = null ){

        switch (true) {
            case $data instanceof \System\Exceptions\RoutingException:
                http_response_code(404);
                break;
            case $data instanceof \System\Exceptions\SecurityException:
                http_response_code(401);
                break;
            case $data instanceof \Exception:
                \System\Logger::error(get_class($data), $data->getMessage());
                http_response_code(500);
                break;
            default: 
                echo $data;
        }

    }

    public function getHeaders(){

        return array(
            'Content-Type' => "text/css"
        );

    }

}
