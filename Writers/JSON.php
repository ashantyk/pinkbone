<?php 
namespace System\Writers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Writers\AbstractWriter;

class JSON extends AbstractWriter {

    public function output( $data, $template = null ){
        
        switch (true) {

            case $data instanceof \System\Exceptions\RoutingException:
                http_response_code(404);
                $data =  array(
                    "title" => _("404 - Page not found"),
                    "message" => _("We could not find the requested page on our servers.")
                );
                break;

            case $data instanceof \System\Exceptions\SecurityException:
                http_response_code(401);
                $data =  array(
                    "title" => _("401 - Unauthorized"),
                    "message" => _("You are not allowed to access this resources.")
                );
                break;

            case $data instanceof \Exception:
                \System\Logger::error(get_class($data), $data->getMessage());
                http_response_code(500);
                $data =  array(
                    "title" => _("500 - Internal Server Error"),
                    "message" => _("A server error occured. An issue has been logged and will be looked into it.")
                );
                break;
                
        }

        switch(true){
            case is_array($data) || is_object($data):
                echo json_encode($data);
                break;
            case is_bool($data):
                echo $data ? "true" : "false";
                break;
            case is_numeric($data):
                echo $data;
                break;
            case is_string($data):
                echo '"' . str_replace('"', '\\"', $data) . '"';
                break;
            case is_null($data):
                echo 'null';
                break;
            default:
                // invalid $data ?
                break;
        }
    
    }

    public function getHeaders(){

        // $origin = $_SERVER['HTTP_ORIGIN'];
        // $allow = preg_match("/domain\.com/", $origin);

        return array(
            'Content-Type' => "application/json",
            // 'Access-Control-Allow-Origin'      => $allow ? $_SERVER['HTTP_ORIGIN'] : null,
            // 'Access-Control-Allow-Credentials' => 'true',
            // 'Access-Control-Allow-Methods'     => 'GET, POST, PUT, DELETE',
            // 'Access-Control-Allow-Headers'     => 'Content-Type, X-Requested-With, Cookie'
        );

    }

}
