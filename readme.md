# PinkBone

PHP Framework for Advanced Web Applications

## Getting Started

1. Create the following directory structure
```
.
├── App/
|   ├── Controller/
|   ├── Record/
|   ├── Store/
|   └── View/
├── Logs/
├── System/
└── index.php
```
2. Clone this repo into the **System** folder

3. Add the following content into the **index.php** file:
```
<?php
// define base path
define("BASEPATH", __DIR__);

// load autoloader
require_once("System/AutoLoader.php");
require_once("System/ErrorHandler.php");

// start application
$app = \System\Application::getInstance();
$app->run();
```

### Prerequisites

You will need the following software requirements to be able to run this framework:
* **PHP** - *7.0+*


### Installing

Your will need to configure your app settings. For this create the file *System/Config.php* and just copy the contents of *System/Config.template.php*. Update values if necessary.


## Authors

* **Gabriel Barbieru** - [Bitbucket](https://bitbucket.com/ashantyk)

## License

This project is licensed under the MIT License
