<?php 
namespace System\Base;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class AbstractStore {

    protected $_app = null;

    public $db = null;

    public function __construct( &$app ){

        $this->_app =& $app;
        $this->db =& $app->getDb();

    }

    public function &getApp(){
        return $this->_app;
    }

    public function &getStore( $modelName ){
        return $this->getApp()->getStore( $modelName );
    }

}
