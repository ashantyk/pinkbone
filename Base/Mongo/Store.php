<?php 
namespace System\Base\Mongo;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Base\AbstractStore;
use \MongoDB\Driver\Query as MongoQuery;
use \MongoDB\Driver\BulkWrite as MongoBulkWrite;
use \MongoDB\Driver\Command as MongoCommand;
use \MongoDB\BSON\ObjectID as MongoObjectID;
use \MongoDB\BSON\UTCDateTime as MongoDate;

abstract class Store extends AbstractStore {

    protected $primaryKeyField = "_id";
    protected $collection = "test";
    protected $itemClass = null;

    const SORT_ASC  = 1;
    const SORT_DESC = -1;

    public function findOne($filter, $projection = [], $options = []){

        $queryOptions = [
            'limit' => 1,
            'singleBatch' => true,
            'batchSize' => 1
        ];

        if(!empty($projection)){
            $queryOptions['projection'] = $projection;
        }
        
        if(!empty($options['sort'])){
            $queryOptions['sort'] = $options['sort'];
        }

        $query = new MongoQuery($filter, $queryOptions);

        $result = $this->db->query($query, [
            'collection' => $this->getCollectionName(),
            'itemClass' => $this->getItemClass(),
            'rawFilter' => $filter
        ]);
        
        return $result ? array_pop($result) : null;

    }

    public function find($filter, $projection = [], $options = []){

        $queryOptions = [
            'projection' => $projection,
            'limit' => !empty($options['limit']) ? $options['limit'] : 0,
            'skip' => !empty($options['skip']) ? $options['skip'] : 0
        ];

        if(!empty($options['sort'])){
            $queryOptions['sort'] = $options['sort'];
        }

        $query = new MongoQuery($filter, $queryOptions);

        $result = $this->db->query($query, [
            'collection' => $this->getCollectionName(),
            'itemClass' => $this->getItemClass(),
            'rawFilter' => $filter
        ]);

        return $result;

    }

    public function count($filter = []){

        $document = [ 
            'count' => $this->getCollectionName()
        ];

        if(!empty($filter)){
            $document['query'] = $filter; 
        }

        $command = new MongoCommand($document);

        $result = $this->db->query($command);

        return $result['n'];

    }

    public function updateOne($filter, $modifications, $params = []){

        $queryOptions = [
            'multi' => false,
            'upsert' => false
        ];

        $update = ['$set' => []];

        if(!empty($params['replace'])){
            $update['$set'] = $this->_flatten($modifications);
        } else {
            $update['$set'] = $modifications;
        }

        $bulk = new MongoBulkWrite();
        $bulk->update($filter, $update, $queryOptions);

        $result = $this->db->query($bulk, [
            'collection' => $this->getCollectionName()
        ]);

        return $result->getModifiedCount() > 0;

    }

    public function updateCustom($filter, $modifications, $params = []){

        $queryOptions = [];

        if(array_key_exists('upsert', $params)){
            $queryOptions['upsert'] = $params['upsert'];
        }

        if(array_key_exists('multi', $params)){
            $queryOptions['multi'] = $params['multi'];
        }

        $bulk = new MongoBulkWrite();
        $bulk->update($filter, $modifications, $queryOptions);

        $result = $this->db->query($bulk, [
            'collection' => $this->getCollectionName()
        ]);

        return $result->getModifiedCount();

    }

    public function updateItemById($modifications, $id, $params = []){

        if(is_string($id)){
            $id = new MongoObjectID($id);
        }

        $filter = [
            $this->primaryKeyField => $id
        ];

        return $this->updateOne($filter, $modifications, $params);

    }

    public function getItemById($id){
        if(is_string($id)){
            $id = new MongoObjectID($id);
        }
        return $this->findOne([$this->primaryKeyField => $id]);
    }

    public function removeItemById($id){

        $filter = [$this->primaryKeyField => $id];
        $options = ['limit' => 1];

        $bulk = new MongoBulkWrite();
        $bulk->delete($filter, $options);

        $result = $this->db->query($bulk, [
            'collection' => $this->getCollectionName()
        ]);

        return $result->getDeletedCount();
    }

    public function getItemClass(){

        if(!$this->itemClass){
            return null;
        }

        return $this->getApp()->getConfig("app", "folder") . "\\Record\\" . $this->itemClass;

    }

    public function getCollectionName(){
        return $this->collection;
    }

    protected function _flatten($array, $prefix = ""){

        $result = [];

        foreach ($array as $key => $value){

            $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

            if (is_array($value)){
                $result = array_merge($result, $this->_flatten($value, $new_key));
            } else {
                $result[$new_key] = $value;
            }
        }

        return $result;

    }

    public function insertOne($document){

        $bulk = new MongoBulkWrite();

        if(is_array($document)){
            $id = $bulk->insert($document);
            if(!array_key_exists($this->primaryKeyField, $document)){
                $document[$this->primaryKeyField] = $id;
            }
        } else {
            $data = $document->getData();
            $id = $bulk->insert($document);
            if(!$document->getID()){
                $document->setID($id);
            }
        }

        $result = $this->db->query($bulk, [
            'collection' => $this->getCollectionName()
        ]);

        if($result->getInsertedCount() === 0){
            throw new \Exception("Failed to insert document.");
        }

        return $document;
    
    }

    public function insertMany($documents){

        $bulk = new MongoBulkWrite();

        foreach ($documents as $key => $document) {
            if(is_array($document)){
                $id = $bulk->insert($document);
                if(!array_key_exists($this->primaryKeyField, $document)){
                    $documents[$key][$this->primaryKeyField] = $id;
                }
            } else {
                $data = $document->getData();
                $id = $bulk->insert($document);
                if(!$document->getID()){
                    $documents[$key]->setID($id);
                }
            }
        }

        $result = $this->db->query($bulk, [
            'collection' => $this->getCollectionName()
        ]);

        if($result->getInsertedCount() === 0){
            throw new \Exception("Failed to insert documents.");
        }

        return $documents;
    }

    public function toMongoDate($unixtimestamp){
        return new MongoDate($unixtimestamp * 1000);
    }

}
