<?php 
namespace System\Base\Mongo;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class Record implements \JsonSerializable {

    protected static $idField = '_id';

    protected $_data;

    public function __construct($data){
        $this->_data = $data;
    }

    public function getID(){
        return $this->get(static::$idField);
    }

    public function setID($id){
        $this->set(static::$idField, $id);
    }

    public function get($property){
        return array_key_exists($property, $this->_data) ? $this->_data[$property] : null;
    }

    public function set($property, $value){
        $this->_data[$property] = $value;
    }

    public function getData(){
        return $this->_data;
    }

    public function jsonSerialize() {
        return $this->_data;
    }

}
