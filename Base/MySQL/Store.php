<?php 
namespace System\Base\MySQL;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Base\AbstractStore;

abstract class Store extends AbstractStore {

    protected $primaryKeyField = "id";
    protected $tableName = "test";
    protected $itemClass = null;

    const ARRAY_DELIMITER = 0x241F;
    const SORT_ASC  = "ASC";
    const SORT_DESC = "DESC";

    /**
     * Make a value safe for adding it to a query
     * Note: This is not the same as the db escape function
     *
     * @param mixed value
     * @return mixed
     */
    protected function _getSafeValue($value){

        if(is_float($value)){
            return "'" . doubleval($value) . "'";
        }

        if(is_int($value)){
            return intval($value);
        }

        if(is_null($value)){
            return "NULL";
        }

        return "'" . $this->db->escape($value) . "'";

    }

    /**
     * Generates the condition for an 'elastic' search
     *
     * @param string $term
     * @param array $fields
     * @return string
     */
    protected function _generateSearchQueryFragment($fields, $term){

        $db =& $this->db;

        // if one word
        if(strpos($term, " ") === false){

            return "(" . implode(" OR ", array_map(function($field) use (&$db, $term){
                return "`" . $db->escape($field) . "` LIKE '%" . $db->escape($term) . "%'";
            }, $fields)) . ")";
            
        }

        // more than one word

        $possibleWords = explode(" ", preg_replace("/[^A-Za-z0-9 -]/", ' ', $term));
        $words = array();

        foreach ($possibleWords as $possibleWord) {

            $possibleWord =  trim($possibleWord);

            if(strlen($possibleWord) <= 2){
                continue;
            }

            $words[] = $possibleWord;

        }

        if(!$words){
            return "";
        }

        return "MATCH(" . implode(", ", array_map(function($field) use (&$db){
            return "`" . $db->escape($field) . "`";
        }, $fields)) . ") AGAINST('" . $this->db->escape(implode(" ", $words)) . "' IN BOOLEAN MODE)";

    }

    /**
     * Inserts a row into the db
     *
     * @param array $record
     * @param array $options
     * @return mixed The id of the inserted document
     */
    public function insertItem($record, $options = array()){

        if(!$record){
            return null;
        }

        $this->insertItems(array($record), $options);
        
        return $this->db->getLastId();

    }

    /**
     * Inserts a row into the db
     *
     * @param array $record
     * @param array $options
     * @return mixed The id of the inserted document
     */
    public function insertItems($records, $options = array()){

        if(!$records){
            return null;
        }

        $options = array_merge(array(
            'ignore' => false
        ), $options);

        $fields = array_keys($records[key($records)]);
        
        $query = "INSERT";

        if($options['ignore']){
            $query .= " IGNORE";
        }

        $query .= " INTO " . $this->db->escape($this->tableName) . " (`" . implode("`, `", $fields) . "`) VALUES ";
        
        $sets = array();
        foreach ($records as $record) {
            $sets[] = "(" . implode(", ", array_map(array($this, "_getSafeValue"), $record)) . ")";
        }

        $query .= implode(", ", $sets);

        $this->db->query($query);

        return $this->db->countAffected();

    }

    /**
     * Deletes a row from the db by its id
     *
     * @param mixed $id
     * @return bool
     */
    public function deleteItemById($id){

        $query = "DELETE FROM " . $this->db->escape($this->tableName) . " WHERE `" . $this->db->escape($this->primaryKeyField) . "` = " . $this->_getSafeValue($id) . " LIMIT 1";

        $this->db->query($query);

        return !!$this->db->countAffected();

    }

    /**
     * Deletes rows from the db by their ids
     *
     * @param array $ids
     * @return int
     */
    public function deleteItemsByIds($ids){

        $query = "DELETE FROM " . $this->db->escape($this->tableName);
        $query .= " WHERE `" . $this->db->escape($this->primaryKeyField) . "` IN (" . implode(", ", array_map(array($this, "_getSafeValue"), $ids)) . ")";

        $this->db->query($query);

        return !!$this->db->countAffected();

    }

    /**
     * Gets a row from the db by its id
     *
     * @param mixed $id
     * @return array|null
     */
    public function getItemById($id){

        $query = "SELECT * FROM " . $this->db->escape($this->tableName) . " WHERE `" . $this->db->escape($this->primaryKeyField) . "` = " . $this->_getSafeValue($id) . " LIMIT 1";

        $result = $this->db->query($query, array(
            "itemClass" => $this->getItemClass()
        ));

        if(!$result->num_rows){
            return null;
        }

        return $result->row;

    }

    /**
     * Gets all the rows from the db
     *
     * @param array $fields Select only certain columns to be returned
     * @return array
     */
    public function getAll( $fields = array() ){

        $query = "SELECT " . ($fields ? "`" . implode("`, `", $fields) . "`" : "*") . " FROM " . $this->db->escape($this->tableName);

        $result = $this->db->query($query, array(
            "itemClass" => $this->getItemClass()
        ));

        if(!$result->num_rows){
            return array();
        }

        return $result->rows;

    }

    /**
     * Update a row in the db by its id
     *
     * @param array $record
     * @param mixed $id This parameter can be skipped if the id is in the $record param
     * @return bool
     */
    public function updateItemById($record, $id = null){

        if(!$id && isset($record[$this->primaryKeyField])){
            $id = $record[$this->primaryKeyField];
        }

        if(!$id){
            throw new \Exception("Missing primaryKey field from record");
        }

        if(isset($record[$this->primaryKeyField])){
            unset($record[$this->primaryKeyField]);
        }

        $query = "UPDATE " . $this->db->escape($this->tableName) . " SET ";
        $updateFragments = array();

        foreach ($record as $key => $value) {
            $updateFragments[] = "`" . $this->db->escape($key) . "` = " . $this->_getSafeValue($value);
        }

        $query .= implode(", ", $updateFragments);

        $query .= " WHERE `" . $this->db->escape($this->primaryKeyField) . "` = " . $this->_getSafeValue($id) . " LIMIT 1";

        $this->db->query($query);

        return !!$this->db->countAffected();

    }

    public function getItemClass(){

        if(!$this->itemClass){
            return null;
        }

        return $this->getApp()->getConfig("app", "folder") . "\\Record\\" . $this->itemClass;

    }

}
