<?php 
namespace System\Base\MySQL;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class Record {

    protected static $idField = 'id';

    public function getID(){
        return $this->get(static::$idField);
    }

    public function get($property){
        return $this->{$property};
    }

    public function set($property, $value){
        $this->{$property} = $value;
    }

}
