<?php 
namespace System\Base;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class AbstractController {

    protected $_app = null;

    public function __construct( &$app ){
        $this->_app =& $app;
    }

    /**
     * Gets the app instance
     *
     * @return \System\Application
     */
    public function &getApp(){
        return $this->_app;
    }

    public function &getStore($modelName){
        return $this->getApp()->getStore($modelName);
    }

    public function loadView($viewName, $data = array(), $template = null){
        return $this->getApp()->loadView($viewName, $data, $template);
    }

}
