<?php 
namespace System\Base;

if(!defined("BASEPATH")) die("Restricted access!");

/**
 * Abstract Security Manager Class
 *
 */
abstract class AbstractSecurityManager {

    /**
     * @var The application instance
     */
    protected $_app;

    /**
     * @var The current user record (if authenticated), null otherwise 
     */
    protected $_currentUser = null;

    /**
     * The Security Manager class constructor
     *
     * @param \System\Application $app
     */
    public function __construct( &$app) {

        $this->_app =& $app;

    }

    /**
     * Returns the current user record (or null if unauthenticated)
     *
     * @return mixed 
     */
    public function getCurrentUser(){
        return $this->_currentUser;
    }

    /**
     * Sets a record as the current user (set to 'null' to become deauthenticated)
     *
     * @param mixed $user
     */
    public function setCurrentUser($user){
        $this->_currentUser = $user;
    }

    /**
     * Checks if the current user has the required rights
     *
     * @param array $rights
     * @return bool
     */
    public abstract function currentUserHasRights( $rights );

    /**
     * Checks if the current session is authenticated
     *
     * @return bool
     */
    public function isAuthenticated(){
        return !($this->_currentUser == null);
    }

    /**
     * Authenticate user
     *
     * @param string $user
     * @param string $password
     * @param bool $remember
     * @return bool
     */
    public abstract function authenticate( $user, $password, $remember = false );

    /**
     * Deauthenticate user
     *
     */
    public abstract function deauthenticate();

    /**
     * Get app instance
     *
     * @return \System\Application
     */
    public function getApp(){
        return $this->_app;
    }

}
