<?php 
namespace System\Base;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Base\AbstractStore;

/**
 * Abstract class that should be used to define Settings-type store for the Application
 *
 */
abstract class AbstractSettingsStore extends AbstractStore {

    /**
     * Get all settings records from the store
     *
     * @param bool $ignoreAutoLoadFlags Setting this to TRUE will make the results to include the records that have autoload = false 
     * @return array
     */
    abstract public function getAll( $ignoreAutoLoadFlags = false );

    /**
     * Update / Create a specific setting
     *
     * @param string $group
     * @param string $name
     * @param mixed $value
     * @param bool|null $autoload
     * @return bool
     */
    abstract public function set($group, $name, $value, $autoload = null);

    /**
     * Gets a specific setting
     *
     * Note: This function will return 'null' if the group/key does not exist
     *
     * @param string $group
     * @param string $name
     * @return mixed
     */
    abstract public function get($group, $name = null);

}
