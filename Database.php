<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

class Database {

    private $_driver = null;

    public function __construct($config) {
      
        $driver = __NAMESPACE__ . '\\Drivers\\' . $config['driver'];

        $this->_driver = new $driver($config['hostname'], $config['port'], $config['database'], $config['username'], $config['password']);
    
    }
        
    public function query($command, $params = array()) {
        return $this->_driver->query($command, $params);
    }

    public function escape($value) {
        return $this->_driver->escape($value);
    }
    
    public function countAffected() {
        return $this->_driver->countAffected();
    }

    public function getLastId() {
        return $this->_driver->getLastId();
    }

    public function getClientVersion() {
        return $this->_driver->getClientVersion();
    }

    public function getServerVersion() {
        return $this->_driver->getServerVersion();
    }

}
