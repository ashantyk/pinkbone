<?php 
namespace System\Readers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Readers\AbstractReader;
use \System\Exceptions\JsonRpcException;

class JsonRPC extends AbstractReader {

    protected $_id = null;
    protected $_version = null;

    public function __construct(){

        $payload = file_get_contents("php://input");
        $payload = json_decode($payload, true);

        if(!is_array($payload)){
            throw new JsonRpcException(_("Invalid request."), JsonRpcException::INVALID_REQUEST);
        }

        if(!isset($payload['id'])){
            throw new JsonRpcException("Missing 'id' parameter", JsonRpcException::INVALID_PARAMS);
        }

        if(!isset($payload['jsonrpc'])){
            throw new JsonRpcException("Missing 'jsonrpc' parameter", JsonRpcException::INVALID_PARAMS);
        }

        if(!isset($payload['method'])){
            throw new JsonRpcException("Missing 'method' parameter", JsonRpcException::INVALID_PARAMS);
        }

        if(strpos($payload['method'], "/") !== false){
            list($class, $method) = explode("/", $payload['method']);
            $this->_class  = ucfirst(trim($class));
            $this->_method = lcfirst(trim($method));
        } else {
            $this->_class  = ucfirst($payload['method']);
            $this->_method = null;
        }

        $this->_params['params'] = isset($payload['params']) && is_array($payload['params']) ? $payload['params'] : array();

        $this->_id = $payload['id'];
        $this->_version = $payload['jsonrpc'];

    }

    public function getID(){
        return $this->_id;
    }

    public function getVersion(){
        return $this->_version;
    }

}
