<?php 
namespace System\Readers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Readers\AbstractReader;

class CLI extends AbstractReader {

    public function __construct(){

        global $argv;

        if(isset($argv[1])){
            $this->_class = $argv[1];
        }

        if(isset($argv[2])){
            $this->_method = $argv[2];
        }

    }

}

