<?php 
namespace System\Readers;

if(!defined("BASEPATH")) die("Restricted access!");

abstract class AbstractReader {

    protected $_class  = null;
    protected $_method = null;
    protected $_params = array();

    public function getClassName(){
        return $this->_class;
    }
    
    public function getMethod(){
        return $this->_method;
    }
    
    public function getParams(){
        return $this->_params;
    }

}
