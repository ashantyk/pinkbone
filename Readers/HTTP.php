<?php 
namespace System\Readers;

if(!defined("BASEPATH")) die("Restricted access!");

use \System\Readers\AbstractReader;
use \System\Exceptions\RoutingException;

class HTTP extends AbstractReader {

    public function __construct(){

        $path         = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $pathSegments = isset($path) ? explode("/", trim($path, "/")) : array();

        switch(true){
            case isset($_GET['class']) && trim($_GET['class']):
                $this->_class = ucfirst(trim($_GET['class']));
                break;
            case isset($pathSegments[0]) && trim($pathSegments[0]):
                $this->_class = ucfirst(trim($pathSegments[0]));
                break;
            default:
                $this->_class = null;
                break;
        }

        switch(true){
            case isset($_GET['method']) && trim($_GET['method']):
                $this->_method = lcfirst(trim($_GET['method']));
                break;
            case isset($pathSegments[1]) && trim($pathSegments[1]):
                $this->_method = lcfirst(trim($pathSegments[1]));
                break;
            default:
                $this->_method = null;
                break;
        }

        $queryString = $_SERVER['QUERY_STRING'];// parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        $params = array();

        if($_SERVER['REQUEST_METHOD'] != "GET" && isset($_SERVER["CONTENT_TYPE"])){

            if(strtolower($_SERVER["CONTENT_TYPE"]) == "application/json"){
                $payload = file_get_contents("php://input");
                $payload = json_decode($payload, true);
            }

            if(strpos(strtolower($_SERVER["CONTENT_TYPE"]), "application/x-www-form-urlencoded") === 0){
                $payload = $_POST;
            }
        
        }


        $queryStringSegments = $queryString ? explode("&", $queryString) : array();
        foreach ($queryStringSegments as $pair) {

            $pairParts = explode("=", $pair);

            if(in_array(strtolower($pairParts[0]), array("class", "method"))){
                continue;
            }

            if(isset($pairParts[1])){
                $params[$pairParts[0]] = urldecode($pairParts[1]);
            }

        }

        if($_POST){
            $params = array_merge($params, $_POST);
        }

        if(isset($payload)){
            $params = array_merge($params, $payload);
        }

        $this->_params = $params;

    }

}
