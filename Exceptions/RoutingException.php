<?php 
namespace System\Exceptions;

if(!defined("BASEPATH")) die("Restricted access!");

class RoutingException extends \Exception {

    const CLASS_NOT_FOUND     = 1;
    const METHOD_NOT_FOUND    = 2;
    const RESOURCE_NOT_FOUND  = 3;
    const MISSING_PARAMETER   = 4;

    const OTHER               = 10;

}
