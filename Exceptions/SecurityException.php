<?php 
namespace System\Exceptions;

if(!defined("BASEPATH")) die("Restricted access!");

class SecurityException extends \Exception {

    const NOT_ENOUGH_RIGHTS   = 2;

}
