<?php 
namespace System\Exceptions;

if(!defined("BASEPATH")) die("Restricted access!");

class JsonRpcException extends \Exception {

    const PARSE_ERROR      = -32700; // Invalid JSON was received by the server. / An error occurred on the server while parsing the JSON text.
    const INVALID_REQUEST  = -32600; // The JSON sent is not a valid Request object.
    const METHOD_NOT_FOUND = -32601; // The method does not exist / is not available.
    const INVALID_PARAMS   = -32602; // Invalid method parameter(s).
    const INTERNAL_ERROR   = -32603; // Internal JSON-RPC error.
    
    // -32000 to -32099 => Server error: Reserved for implementation-defined server-errors.

    const UNAUTHORIZED     = -32000; 

}
