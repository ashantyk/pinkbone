<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

class CacheManager {

    private $driver = null;

    public function __construct($config) {
      
        if($config['enabled'] /*&& $_SERVER['REMOTE_ADDR'] != "188.27.67.80"*/){
            $driverClass = __NAMESPACE__ . '\\Drivers\\' . $config['driver'];
            $this->driver = new $driverClass($config);
        }
    
    }
        
    public function get($key) {

        if(!$this->driver){
            return null;
        }

        return $this->driver->get($key);

    }
    
    public function set($key, $value, $ttl = 86400) {

        if(!$this->driver){
            return null;
        }

        return $this->driver->set($key, $value, $ttl);

    }
    
    public function clear($key) {

        if(!$this->driver){
            return null;
        }

        return $this->driver->clear($key);
    
    }

    public function empty() {
        
        if(!$this->driver){
            return null;
        }

        return $this->driver->empty();

    }

    public function buildCacheKey($class, $method, $params){

        $key = $class . "_" . $method;

        if($params){
            $key .= "_" . str_replace("%20", "_", http_build_query($params, "", "_")); 
        }

        return $key;
        
    }

    public function getStatus(){

        if(!$this->driver){
            return false;
        }

        return $this->driver->getStatus();
    }

}
