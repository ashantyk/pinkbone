<?php 

// register error handler
function app_error_handler( $errorCode, $errorMessage, string $errorFile, $errorFileLine, $errorContext){
    \System\Logger::error("URI: " . $_SERVER['REQUEST_URI']);
    \System\Logger::error($errorMessage, "[" . $errorCode . "]", "in file:", $errorFile . ":" . $errorFileLine /*, "Context:", $errorContext*/ );
}
set_error_handler("app_error_handler");

// register shutdown handler
function app_shutdown_handler(){
    $lastError = error_get_last();
    switch ($lastError['type']){
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            \System\Logger::error("URI: " . $_SERVER['REQUEST_URI']);
            \System\Logger::error($lastError['message'], "[" . $lastError['type'] . "]", "in file:", $lastError['file'] . ":" . $lastError['line'] /*, "Context:", $lastError['context']*/ );
    }
}
register_shutdown_function("app_shutdown_handler");

// // register exception handler 
function app_exception_handler( $exception ){
    \System\Logger::error("URI: " . $_SERVER['REQUEST_URI']);
    \System\Logger::error("Uncaught exception: " . $exception->getMessage() . " [" . $exception->getCode() . "] in file: " . $exception->getFile() . ":" . $exception->getLine() /*, "Context:", $exception->getTraceAsString() */ );
}
set_exception_handler("app_exception_handler");
