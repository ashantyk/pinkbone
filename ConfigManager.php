<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

class ConfigManager {

    /**
     * @var array This is where the config settings are stored for the app instance
     */
    protected $_config;

    /**
     * @var \System\Application The app instance
     */
    protected $_app;

    /**
     * Config Mananger class constructor
     *
     * @param \System\Application $app
     * @throws \Exception
     */
    public function __construct(&$app) {

        $this->_app =& $app;

        $configPath = __DIR__ . DIRECTORY_SEPARATOR . "Config.php";

        if(!file_exists($configPath)){
            throw new \Exception("Missing configuration file.");
        }

        $config = array();
        include($configPath);
        $this->_config = $config;
        unset($config);

    }

    /**
     * Load additional settings from a settings-type store
     *
     * @throws \Exception
     */
    public function initDatabaseSettings(){

        if(!isset($this->_config['app']['settingsStore'])){
            return;
        }

        $groupedConfigs = $this->_app->getStore($this->_config['app']['settingsStore'])->getAll();

        if(!$groupedConfigs){
            throw new \Exception("Failed to load app configuration from database!");
        }

        foreach ($groupedConfigs as $groupName => $groupData) {

            if(!isset($this->_config[$groupName])){
                $this->_config[$groupName] = array();
            }

            $this->_config[$groupName] = array_merge($groupData, $this->_config[$groupName]);

        }
    }

    /**
     * Gets config(s)
     *
     * @param string|null $group
     * @param string|null $name
     * @return mixed
     */
    public function get($group = null, $name = null){

        if(is_null($group)){
            return $this->_config;
        }

        if(is_string($group) && is_null($name) && array_key_exists($group, $this->_config)){
            return $this->_config[$group];
        }

        $refreshedGroup = false;

        if(!array_key_exists($group, $this->_config)){

            if(isset($this->_config['app']['settingsStore'])){
                $this->_config[$group] = $this->_app->getStore($this->_config['app']['settingsStore'])->get($group);
            }

            if(is_null($name)){
                return $this->_config[$group];
            }

            $refreshedGroup = true;
            
        }

        if(is_array($this->_config[$group])){

            if(!array_key_exists($name, $this->_config[$group])) {

                if($refreshedGroup){
                    return null;
                }

                if(isset($this->_config['app']['settingsStore'])){
                    $this->_config[$group][$name] = $this->_app->getStore($this->_config['app']['settingsStore'])->get($group, $name);
                }

            }

            return $this->_config[$group][$name];

        }

        return null;

    }

    /**
     * Sets a config
     *
     * @param string $group
     * @param string $name
     * @param mixed $value
     * @param bool|null $autoload
     * @return bool
     */
    public function set($group, $name, $value, $autoload = null){

        if(!array_key_exists($group, $this->_config)){
            $this->_config[$group] = array();
        }

        $this->_config[$group][$name] = $value;

        if(isset($this->_config['app']['settingsStore'])){
            return $this->_app->getStore($this->_config['app']['settingsStore'])->set($group, $name, $value, $autoload);
        }

        return true;
        
    }

}
