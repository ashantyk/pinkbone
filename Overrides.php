<?php if(!defined("BASEPATH")) die("Restricted access!");

function e($str, $output = true){
    if($output){
        echo htmlspecialchars($str);
    } else {
        return htmlspecialchars($str);
    }
}

function _n($single, $plural, $number){
    return str_replace('%d', $number, ngettext($single, $plural, $number));
}
