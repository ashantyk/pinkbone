<?php
namespace System\Drivers;

if(!defined("BASEPATH")) die("Restricted access!");

class FileCache {

    protected $_config;

    public function __construct($config = array()) {
        $this->_config = $config;
    }

    function _getFilePathByKey($key){
        $key = str_replace(array("\\", ".", "\0"), "_", $key);
        return $this->_config['dir'] . DIRECTORY_SEPARATOR . $key . ".cache";
    }

    public function get( $key ){
        
        // Build the file path.
        $filepath = $this->_getFilePathByKey($key);

        // Check if the cache exists, if not return FALSE
        if (!@file_exists($filepath)){
            return null;
        }

        // check if cache expired. if so, return FALSE
        if(filemtime($filepath) < time()){
            @unlink($filepath);
            return null;
        }

        // get file contents
        $data = file_get_contents($filepath);

        if(!$data){
            // return false if empty
            return null;
        }

        // return file contents
        return unserialize($data);

    } 

    public function set( $key, $value, $ttl ){

        // build the file path.
        $filepath = $this->_getFilePathByKey($key);

        // save data to disk
        $bytesWritten = file_put_contents($filepath, serialize($value), LOCK_EX);

        touch($filepath, time() + $ttl);

        return (bool)$bytesWritten;
        
    } 


    public function clear( $key ){

        // build the file path.
        $filepath = $this->_getFilePathByKey($key);

        if(!file_exists($filepath)){
            return true;
        }

        // delete cache file
        return unlink($filepath);

    }

    public function empty(){
        // glob all .cache files and delete them
        array_map('unlink', glob($this->_config['dir'] . DIRECTORY_SEPARATOR . "*.cache"));
    }

    public function getStatus(){
        
        $iterator = new \FilesystemIterator($this->_config['dir'], \FilesystemIterator::SKIP_DOTS);

        $totalFiles = 0;
        $totalSize = 0;
        foreach ($iterator as $fileInfo) {
            if($fileInfo->isFile()){
                $totalFiles++;
                $totalSize += $fileInfo->getSize();
            }
        }

        return array(
            "type" => "FileSystem",
            "files" => $totalFiles,
            "size" => \App\Library\Utils::friendlySize($totalSize)
        );

    }

}
