<?php
namespace System\Drivers;

if(!defined("BASEPATH")) die("Restricted access!");

final class MySQLi {

    private $_link;

    public function __construct($hostname, $port, $database, $username, $password) {

        $this->_link = @new \mysqli($hostname, $username, $password, $database, $port);

        if (\mysqli_connect_error()) {
            throw new \ErrorException('Error: Could not make a database link (' . \mysqli_connect_errno() . ') ' . \mysqli_connect_error());
        }

        $this->_link->set_charset("utf8");
        $this->_link->query("SET SQL_MODE = ''");

    }

    public function query($sql, $params = array()) {

        $query = $this->_link->query($sql);

        if (!$this->_link->errno){

            if (isset($query->num_rows)) {

                $data = array();

                while ($row = isset($params['itemClass']) ? $query->fetch_object($params['itemClass']) : $query->fetch_assoc()) {
                    $data[] = $row;
                }

                $result = new \stdClass();
                $result->num_rows = $query->num_rows;
                $result->row = isset($data[0]) ? $data[0] : array();
                $result->rows = $data;

                unset($data);

                $query->close();

                return $result;

            } else {

                return true;

            }

        } else {
            throw new \ErrorException('Error: ' . $this->_link->error . '<br />Error No: ' . $this->_link->errno . '<br />' . $sql);
            //exit();
        }

    }

    public function escape($value) {
        return $this->_link->real_escape_string($value);
    }

    public function countAffected() {
        return $this->_link->affected_rows;
    }

    public function getLastId() {
        return $this->_link->insert_id;
    }

    public function getClientVersion(){
        $versionRaw = $this->_link->client_version; // main_version*10000 + minor_version *100 + sub_version
        $versionReversed = strrev((string)$versionRaw);
        $version = substr_replace($versionReversed, ".", 2, 0);
        $version = substr_replace($version, ".", 5, 0);
        $version = preg_replace("/\.0([0-9])\./", ".\$1.", $version);
        return strrev($version);
    }

    public function getServerVersion(){
        return $this->_link->server_info;
    }

    public function __destruct() {
        $this->_link->close();
    }

}
