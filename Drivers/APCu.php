<?php
namespace System\Drivers;

if(!defined("BASEPATH")) die("Restricted access!");

final class APCu {

    protected $_config;

    public function __construct($config = array()) {
        $this->_config = $config;
    }

    public function get($key){
        
        $value =  apcu_fetch($key, $success);

        return $success === true ? $value : null;

    }

    public function set($key, $value, $ttl){
        return apcu_store($key, $value, $ttl);
    }

    public function clear($key){
        return apcu_delete();
    }

    public function empty(){
        return apcu_clear_cache();
    }

    public function getStatus(){

        return array(
            "type" => "APCu",
            "info" => apcu_cache_info(false)
        );

    }

}
