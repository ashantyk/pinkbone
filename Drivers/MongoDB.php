<?php
namespace System\Drivers;

if(!defined("BASEPATH")) die("Restricted access!");

use \MongoDB\Driver\Manager as MongoManager;
use \MongoDB\Driver\Command as MongoCommand;
use \MongoDB\Driver\Query as MongoQuery;
use \MongoDB\Driver\BulkWrite as MongoBulkWrite;

final class MongoDB {

    private $_manager;
    private $_defaultDatabase;

    private const ADMIN_DB = "admin";

    public function __construct($hostname, $port, $database, $username = null, $password = null) {

        $uri = "mongodb://" . $hostname;

        if(is_integer($port)){
            $uri .= ":" . $port;
        }

        $uri .= "/" . $database;

        $options = array();

        if($username){
            $options['username'] = $username;
        }

        if($password){
            $options['password'] = $password;
        }

        $this->_manager = new MongoManager($uri, $options);
        $this->_defaultDatabase = $database;

        return;

        // test connection
        $command = new MongoCommand(array('ping' => 1));

        try {
            $cursor = $this->_manager->executeCommand($database, $command);
        } catch(\MongoDB\Driver\Exception $ex) {
            throw new \ErrorException('Error: Could not make a database link: ' . $ex->getMessage());
        }

        $response = current($cursor->toArray());

        if(!$response['ok']){
            throw new \ErrorException('Error: Could not successfully ping database: ' . json_encode($response));
        }

    }

    public function query($query, $params = array()) {

        $params = array_merge(array(
            'db' => $this->_defaultDatabase,
            'single' => false,
            'returnCursor' => false
        ), $params);

        if($query instanceof MongoQuery){
            if(empty($params['collection'])){
                throw new \Exception("Collection not specified");
            }
            return $this->_runQuery($params['db'], $params['collection'], $query, $params);
        }

        if($query instanceof MongoBulkWrite){
            if(empty($params['collection'])){
                throw new \Exception("Collection not specified");
            }

            return $this->_runBulkWrite($params['db'], $params['collection'], $query, $params);
        }

        return $this->_runCommand($params['db'], $query, $params);

    }

    private function _runQuery($db, $collection, $query, $params){

        $cursor = $this->_manager->executeQuery($db . "." . $collection, $query);
        $cursor->setTypeMap(['root' => 'array', 'document' => 'array']);

        if($params['returnCursor']){
            return $cursor;
        }

        $documents = array();

        foreach($cursor as $document) {
            $documents[] = isset($params['itemClass']) ? new $params['itemClass']($document) : $document;
        }

        return $documents;

    }

    private function _runBulkWrite($db, $collection, $query, $params){
        return $this->_manager->executeBulkWrite($db . "." . $collection, $query);
    }

    private function _runCommand($db, $command, $params){

        $cursor = $this->_manager->executeCommand($db, $command);

        $cursor->setTypeMap(['root' => 'array', 'document' => 'array']);

        if($params['returnCursor']){
            return $cursor;
        }

        $result = $cursor->toArray();
        return $params['single'] ? $result[0] : $result;

    }

    public function escape($value) {
        // this driver does not need escaping
        return $value;        
    }

    public function countAffected() {
        // todo // return $this->link->affected_rows;
    }

    public function getLastId() {
        // todo // return $this->link->insert_id;
    }

    public function getClientVersion(){
        return phpversion('mongodb');
    }

    public function getServerVersion(){
        $command = new MongoCommand(array("serverStats" => true));
        $result = $this->_manager->executeCommand(static::ADMIN_DB, $command)->toArray();
        $version = current($result)['version'];
        return $version;
    }

    public function __destruct() {
        // this driver does not support closing connections
    }

}
