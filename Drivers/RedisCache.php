<?php
namespace System\Drivers;

if(!defined("BASEPATH")) die("Restricted access!");

final class RedisCache {

    protected $_config;
    protected $_client;

    public function __construct($config = array()) {

        $this->_config = $config;
        $this->_client = new \Redis();

        if(empty($config['persistent'])){
            $this->_client->connect($config['host'], $config['port']);
        } else {
            $this->_client->pconnect($config['host'], $config['port']);
        }
    }

    public function get($key){
        $value = $this->_client->get($this->_getSanitizedKey($key));
        return $value === false ? null : unserialize($value);
    }

    public function set($key, $value, $ttl){
        $result = $this->_client->set($this->_getSanitizedKey($key), serialize($value), $ttl);
        return (bool)$result;
    }

    public function clear($key){
        $result = $this->_client->del($this->_getSanitizedKey($key));
        return (bool)$result;
    }

    public function empty(){
        return $this->_client->flushAll();
    }

    public function getStatus(){

        return array(
            "type" => "Redis",
            "info" => $this->_client->info()
        );

    }

    public function _getSanitizedKey($key){
        return $this->_config['prefix'] . $key;
    }

}
