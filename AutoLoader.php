<?php 

// setup autoloader
spl_autoload_register(function ($class) {
    $path = BASEPATH . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class) . '.php';
    if(file_exists($path)){
        require_once($path);
    }
});

