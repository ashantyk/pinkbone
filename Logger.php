<?php
namespace System;

if(!defined("BASEPATH")) die("Restricted access!");

/**
 * Logger class
 *
 * @method static Logger error(mixed $msg) Log error level message
 * @method static Logger info(mixed $msg) Log info level message
 * @method static Logger debug(mixed $msg) Log debug level message
 * @method static Logger notice(mixed $msg) Log notice level message
 * @method static Logger warning(mixed $msg)Log warning level message
 */
class Logger {

    const SEVERITY_LEVEL_DEBUG   = 0;
    const SEVERITY_LEVEL_INFO    = 1;
    const SEVERITY_LEVEL_NOTICE  = 2;
    const SEVERITY_LEVEL_WARNING = 3;
    const SEVERITY_LEVEL_ERROR   = 4;

    private static $_allowedLevels = array(
        "debug"   => self::SEVERITY_LEVEL_DEBUG,
        "info"    => self::SEVERITY_LEVEL_INFO,
        "notice"  => self::SEVERITY_LEVEL_NOTICE,
        "warning" => self::SEVERITY_LEVEL_WARNING,
        "error"   => self::SEVERITY_LEVEL_ERROR
    );

    private static $_config = null;

    private static function log( $severity, $arguments ){

        // convert custom arguments
        foreach ($arguments as &$argument) {

            if(is_array($argument) || $argument instanceof \JsonSerializable ){
                $argument = json_encode($argument, JSON_PRETTY_PRINT);
            } else if($argument === null){
                $argument = "NULL";
            }

        }
        unset($argument);

        // push date and severity level into log entry
        array_unshift(
            $arguments, 
            "[" . date("d-M-Y H:i:s e") . "]", // [31-Mar-2016 21:17:52 Europe/Bucharest]
            "[" . $severity . "]" // [debug]
        );

        // save log entry to disk
        @file_put_contents(BASEPATH . DIRECTORY_SEPARATOR . strftime(static::$_config['path']), implode(" ", $arguments) . PHP_EOL, FILE_APPEND);

    }

    public static function setConfig($newConfig){
        static::$_config = $newConfig;
    }

    public static function __callStatic($method, $args) {

        // validate method (severity)
        if(!array_key_exists($method, static::$_allowedLevels)){
            throw new \ErrorException("Logger class has no such method: " . $method);
        }

        // lazy load logger config
        if(!static::$_config){
            $app = Application::getInstance();
            static::$_config = $app->getConfig("logger");
        }

        // get level
        $level = static::$_allowedLevels[$method];

        // skip entries lower than the level set in config
        if(static::$_config['level'] > $level){
            return;
        }

        // call logging method
        return static::log($method, $args);

    }

}